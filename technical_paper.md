# Caching

## Contents
* whats is caching
  * Definition
* Selecting the right caching approach
* Benefits of caching
* Caching approaches
  * Cache-Aside Caching
  * Read-Through Caching
  * Write-Through Caching
  * Write-Around Caching
  * Write-Back Caching
* Conclusion
* Reference Links 

#

## What is caching

### Definition
Caching is a ***technique*** to reduce the reading and writing time data to and from a resource. What it does is that it temporarily stores the data at a place from where it can be retrieved at a ***faster*** speed.

#

## Selecting the right caching approach
There are various points to see before selecting a caching approach
* will you be ***reading*** more and ***writing*** less
* will you be ***writing*** more and ***reading*** less
* will the data returned always be ***unique***
* do you ***strictly*** care about cache data and the data in the database to be ***synced***

#

## Benefits of caching
* ***Improve*** application performance
* ***Reduce*** load on the database
* ***Reduce*** database cost by accessing it less frequently
* ***Improving*** UX

#

## Caching approaches
There are mainly three entities to care about in caching. ***Database, Cache, and Application***.

### 1. Cache-Aside Caching
This is used to ***read*** data from the cache. In this, the cache and application are directly connected, and the database is connected to the application. The application looks for data in the cache and if data is not found it asks for data from the database and updates it to cache.

If for some reason the cache fails, the application can directly request data from the database. If too much data is requested from the database it can increase the cost or can even make the application slow. The data model of cache can be different from the data model in the database.

### 2. Read-Through Caching
This is used to ***read*** data from the cache. In this, the cache is between the application and the database. The application interacts with the cache for any data. If the data is not found in the cache then the cache is responsible to request data from the database and update itself.

When the data is requested for the ***first*** time, it may not be able to find it and may take more than usual time to request data from the database as the cache and database are not directly connected. the data model used in the cache and the database should be the same.

> To maintain the data consistency, time to live (TTL) is used. this ensured that after a certain time the cache data will become ***invalid*** and needs to be refreshed.

### 3. Write-Through Caching
It is used to ***write*** data to cache. In this, the cache is in between the database and the application. Write-Through is suitable for ***read heavy*** applications because writing is slow in this technique due to the cache being the one responsible for updating the database. Write-Through caching is usually paired with Read-Through caching.

### 4. Write-Around Caching
It is used to ***write*** data directly to the database and then data is updated in the cache. In this, the data is written directly to the application and only the data which is read is cached. It can be combined with read-through or the cache aside. It is used in situations where data is written once and is read less frequently.

### 5. Write-Back Caching
It is used to write ***data*** to cache, where cache sits in ***between*** application and database. It is good for heavy writing of data to the database as it sends data to cache in chunks, there the data stays for some time and then is updated to the database. If the database takes values in batch then it can greatly reduce the cost of the database. In Write-Back Caching we also get the most ***recently updated*** values all the time. The disadvantage to this is that if the cache fails, we can lose a large amount of data.

#

## Conclusion
There are various caching approaches and we have to choose wisely according to our use case. For a task that requires heavy read cycles such as user ***profiles, image viewing, etc***. we can use read-through or cache aside caching. For a task that requires heavy write cycles such as ***storing log files***, we can use Write-Back caching. Similarly, we can use a ***combination*** of read and write caching according to our use case.

#

## Reference Links
1. [codeahoy.com](https://codeahoy.com/2017/08/11/caching-strategies-and-how-to-choose-the-right-one/)
2. [medium.com](https://medium.datadriveninvestor.com/all-things-caching-use-cases-benefits-strategies-choosing-a-caching-technology-exploring-fa6c1f2e93aa)
3. [oracle.com](https://docs.oracle.com/cd/E15357_01/coh.360/e15723/cache_rtwtwbra.htm#COHDG485)
